<?php
$env = key_exists("e", getopt("e:")) ? getopt("e:")["e"] : "dev";

$priority = ["dev", "test", "ppe", "prod"];

$priority_settings = [];

$settings = [
    "mysqli" => [
        "ppe" => [
            "host" => "localhost-ppe", 
            "user" => "root-ppe", 
            "password" => "pass-ppe"
        ],
        "test" => [
            "host" => "localhost-test", 
            "user" => "root-test", 
            "password" => "pass-test"
        ]
    ],
    "other" => [
        "test" => [
            "email-main" => "email-main-test", 
        ],
        "ppe" => [
            "email-main" => "email-main-ppe", 
        ]        
    ],
];

foreach ($settings as $source => $setting) {    
    if (key_exists($env, $setting)) {
        $priority_settings[$source] = $setting[$env];
    } else {
        array_walk($priority, function ($priority_env) use ($source, $setting, &$priority_settings) {
            if (key_exists($priority_env, $setting) && !count($priority_settings[$source])) {
                $priority_settings[$source] = $setting[$priority_env];
            }
        });
    }
};


print_r($priority_settings); exit();
